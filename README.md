## IRC Server + Web Client

Simple and full-featured irc server, znc and web client as a set of multiple docker images includes :

- **Inspircd** : a full irc server
- **Znc** : an irc network bouncer
- **theLounge** : web based irc client for multiple users
- **Træfik** : modern HTTP reverse proxy
- **SSL** : let's encrypt with auto-renewal

### Prerequisites

Make sure that no other application is interfering with irc server configuration :

```
# netstat -tulpn | grep -E -w '80|443|6697|6667|6699|9000'
```

If this command returns any results please remove or stop the application running on that port.

Ensure that [docker](https://docs.docker.com/install/#server) & [docker-compose](https://docs.docker.com/compose/install/) are installed.

#### Ports

If you have a firewall, unblock the following ports, according to your needs :

| Service | Software | Protocol | Port |
| ------- | -------- | -------- | ---- |
| HTTP | Træfik | TCP | 80 |
| HTTPS | Træfik | TCP | 443 |
| IRC | Inspircd | TCP | 6667 |
| IRC-SSL | Inspircd | TCP | 6697 |
| ZNC | Znc | TCP | 6699 |

#### DNS records:

A correct DNS setup is required, this step is very important.

| HOSTNAME | CLASS | TYPE | PRIORITY | VALUE |
| -------- | ----- | ---- | -------- | ----- |
| irc | IN | A/AAAA | any | 1.2.3.4 |
| monitor | IN | CNAME | any | irc.domain.tld. |

### Installation

#### 1 - Create the required folders and files

```bash
mkdir -p /mnt/docker/traefik/acme \
&& cp -R lounge /mnt/docker/ \
&& cp docker-compose.sample.yml /mnt/docker/docker-compose.yml \
&& cp sample.env /mnt/docker/.env \
&& cp traefik.sample.toml /mnt/docker/traefik/traefik.toml \
&& cd /mnt/docker/ \
&& touch traefik/acme/acme.json \
&& chmod 600 docker-compose.yml .env traefik/traefik.toml traefik/acme/acme.json
```

Create a new docker network for Traefik (IPv4 only)
`docker network create http_network`

Edit the .env, traefik.toml and lounge/config/config.js, adapt to your needs, then start all services :
`docker-compose up -d`

Check startup logs with
`docker logs -f`


#### 2 - ZNC Setup (optional)

TODO: For more info see the [ZNC Introduction](https://wiki.znc.in/Introduction)

#### 3 - theLounge Setup

Add a new user
`docker-compose run lounge lounge add john`

For more info see [theLounge usage and commands](https://thelounge.chat/docs/getting_started/usage.html)

#### 4 - Done, congratulation ! :tada:

At first launch, the container takes few minutes to generate SSL certificates (if needed).

**List of webservices available:**

| Service | URI |
| ------- | --- |
| **Traefik dashboard** | https://monitor.domain.tld/ |
| **theLounge IRC Client** | https://irc.domain.tld/ |



### Todo
* Use letsencrypt cert for ZNC & IRC containers `cat /etc/letsencrypt/live/domain.tld/{privkey,cert,chain}.pem > ~/.znc/znc.pem`
* Improve documentation for configuring ZNC
